/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal1.internal;

import java.util.Dictionary;
import java.util.Properties;
import pk.labs.LabC.logger.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author Adam
 */
public class Activator implements BundleActivator {

    private Logger logger;
    
    @Override
    public void start(BundleContext bc) throws Exception {
        bc.registerService(Animal.class.getName(), new Lew(), (Dictionary)new Properties());
        
        logger.get().log(this, "Witamy Lwa!");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        this.logger.get().log(this, "Zegnamy Lwa!");
    }
    
}
