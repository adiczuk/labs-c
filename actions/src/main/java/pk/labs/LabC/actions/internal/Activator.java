/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;

import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.AnimalAction;

/**
 *
 * @author Adam
 */
public class Activator implements BundleActivator {

    private BundleContext context;

    @Override
    public void start(BundleContext bc) throws Exception {

        context = bc;
        
        Bieganie bieganie = new Bieganie();
        Jedzenie jedzenie = new Jedzenie();
        Spanie spanie = new Spanie();

        Hashtable ht1 = new Hashtable();
        Hashtable ht2 = new Hashtable();
        Hashtable ht3 = new Hashtable();

        ht1.put("name", "Lew");
        ht1.put("name", "Bieganie");
        ht2.put("name", "Malpa");
        ht2.put("name", "Jedzenie");
        ht3.put("name", "Slon");
        ht3.put("name", "Spanie");

        context.registerService(AnimalAction.class.getName(), bieganie, ht1);
        context.registerService(AnimalAction.class.getName(), jedzenie, ht2);
        context.registerService(AnimalAction.class.getName(), spanie, ht3);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        context = null;
    }

}
