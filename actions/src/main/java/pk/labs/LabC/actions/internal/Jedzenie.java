/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

/**
 *
 * @author Adam
 */
public class Jedzenie implements AnimalAction {
    
    private String name;
    
    public Jedzenie(){
        this.name  = "Jedzenie";
    }
    
    @Override
    public boolean execute(Animal animal) {
        animal.setStatus("Je...");
        return true;
    }
    
}
