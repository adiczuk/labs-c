/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal3.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author Adam
 */
public class Slon implements Animal {
    
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    private String species;
    private String name;
    private String status;
    
    public Slon()
    {
        this.species = "Sloniowate";
        this.name = "Slon";
        this.status = "";
    }

    @Override
    public String getSpecies() {
       return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }
    
}
